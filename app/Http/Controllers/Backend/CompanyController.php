<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Prefecture;
use Config;
use DB;
use Storage;

class CompanyController extends Controller {

    
    private function getRoute() {
        return 'company';
    }
    
    protected function validator(array $data, $type) {
        // Determine if password validation is required depending on the calling
        return Validator::make($data, [
                'name' => 'required|string|max:100',
                'email' => 'required|string|max:255',
                'postcode' => 'required|string|max:7',
                'prefecture_id' => 'required|int|max:11',
                'city' => 'required|string|max:255',
                'local' => 'required|string|max:255',
                'street_address' => 'string|max:255',
                'busines_hour' => 'string|max:45',
                'regular_holiday' => 'string|max:45',
                'phone' => 'string|max:15',
                'fax' => 'string|max:15',
                'url' => 'string|max:255',
                'license_number' => 'string|max:255',
                'image_file' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        ]);
    }

    public function index() {
        $prefectures = Prefecture::pluck('display_name', 'id');
        return view('backend.companies.index',compact($prefectures));
    }
    
    public function add()
    {
        $company = new Company();
        $company->form_action = $this->getRoute() . '.create';
        $company->page_title = 'Company Add Page';
        $company->page_type = 'create';
        $prefectures = Prefecture::pluck('display_name', 'id');
        return view('backend.companies.form', [
            'company' => $company,
            'prefectures' => $prefectures
        ]);
    }
    
    public function create(Request $request) {
        $newCompany = $request->all();
        $this->validator($newCompany, 'create')->validate();

        try {
            DB::beginTransaction();
            $company = Company::create($newCompany);
            $file = $request->file('image_file');
            $name = 'images_' . $company->id . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/uploads/files',$name);
            $company->image = 'uploads/files/'.$name;
            $company->save();
            if ($company) {
                DB::commit();
                return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_CREATE_MESSAGE'));
            } else {
                DB::rollBack();
                return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_CREATE_MESSAGE'));
            }
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_CREATE_MESSAGE'));
        }
    }
    
    public function edit($id) {
        $company = Company::find($id);
        $company->form_action = $this->getRoute() . '.update';
        $company->page_title = 'Company Edit Page';
        $company->page_type = 'edit';
        $prefectures = Prefecture::pluck('display_name', 'id');
        return view('backend.companies.form', [
            'company' => $company,
            'prefectures' => $prefectures
        ]);
    }
    
    public function update(Request $request) {
        $newCompany = $request->all();
        try {
            $currentCompany = Company::find($request->get('id'));
            if ($currentCompany) {
                DB::beginTransaction();
                $file = $request->file('image_file');
                $name = 'images_' . $currentCompany->id . '.' . $file->getClientOriginalExtension();
                Storage::delete('public/uploads/files/'.$name);
                $file->storeAs('public/uploads/files',$name);
                $this->validator($newCompany, 'update')->validate();
                $currentCompany->update($newCompany);
                DB::commit();
                return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_UPDATE_MESSAGE'));
            } else {
                DB::rollback();
                return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_UPDATE_MESSAGE'));
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_UPDATE_MESSAGE'));
        }
    }

    public function delete(Request $request) {
        try {
            $company = Company::find($request->get('id'));
            $company->delete();
            return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_DELETE_MESSAGE'));
        } catch (Exception $e) {
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_DELETE_MESSAGE'));
        }
    }

}
