<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Postcode;
use App\Models\Prefecture;

class ApiPrefectureController extends Controller
{
    public function getPrefectureData($postcode) {
        $postcoded = Postcode::where('postcode', $postcode)->first();
        $prefecture = Prefecture::where('display_name', $postcoded->prefecture)->first();
        return response()->json(['postcode' => $postcoded, 'prefecture' => $prefecture]);
    }

    public function getPrefectureList() {
        $prefecture = Prefecture::get();
        return response()->json(['prefecture' => $prefecture]);
    }
}
