$(function() {
    // init: side menu for current page
    $('li#menu-companies').addClass('menu-open active');
    $('li#menu-companies').find('.treeview-menu').css('display', 'block');
    $('li#menu-companies').find('.treeview-menu').find('.add-companies a').addClass('sub-menu-active');

    $('#company-form').validationEngine('attach', {
        promptPosition: 'topLeft',
        scroll: false
    });

    // init: show tooltip on hover
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    // show password field only after 'change password' is clicked
    $('#getPref').click(function(e) {
        var a = $("#postcodeInput").val()
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", rootUrl + "/api/postcode/getPref/" + a, false);
        xmlHttp.send(null);
        var item = JSON.parse(xmlHttp.responseText);
        $('#prefecture_id').val(item.prefecture.id);
        $('#prefecture').val(item.prefecture.display_name);
        $('#city').val(item.postcode.city);
        $('#local').val(item.postcode.local);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#image_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_file").on("change", function() {
        readURL(this);
    });
});